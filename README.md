# Nordmands Problem

Got the challenge to create a program in half an hour that solved this problem:

Oppgaven er: Write a (Python) program that prompts the user to enter a list of words and stores in a list only those words whose first letter occurs again within the word (for example, 'baboon'). The program should display the resulting list.

 Program outcome:

Enter a series of words, one per line (hit return when done)

Enter word: baboon
Enter another word: nintendo
Enter another word: nice

Enter another word: program
Enter another word:
Recorded words: ['baboon', 'nintendo']
 
Jeg har ingen anelse hvor jeg starter. Jeg vet jeg skal bruke list, if/else, input og print (sikkert?).

Kan noen hjelpe meg med å forstå hvordan jeg skal angripe oppgaven? Hvor skal jeg starte? Hvordan vet jeg hva som skal komme først osv.?

Takker på forhånd!
